import http from './index';

export default {
  getBooksList: async (id, pagination) => {
    const { data } = await http.get('illustration', {
      params: {
        id: id || null,
        page: pagination.current,
        count: pagination.pageSize,
      },
    });
    return data;
  },
  getSingleBook: async (id) => {
    const { data } = await http.get(`illustration/${id}`);
    return data;
  },
};

import Axios from 'axios';

const http = Axios.create({
  baseURL: process.env.REACT_APP_BASE_URL || 'https://cors-anywhere.herokuapp.com/http://95.216.159.188:7003/api/',
});

export default http;

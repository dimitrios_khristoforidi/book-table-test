import { Input, Tooltip } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getBooksSearch } from '../../store/actions/booksTableActions';
import { booksSearchList, booksSearchTerm } from '../../store/selectors/booksSelectors';

const { Search } = Input;

export default function SearchField() {
  const dispatch = useDispatch();
  const onSearch = (value) => dispatch(getBooksSearch(value));
  const searchList = useSelector(booksSearchList);
  const searchTerm = useSelector(booksSearchTerm);

  console.log(searchTerm, !searchList.length);

  return (
    <Tooltip
      trigger={['focus']}
      visible={!!(searchTerm && !searchList.length)}
      title="Нет результатов"
      placement="topLeft"
      overlayClassName="numeric-input"
    >
      <Search
        placeholder="Введите текст поиска"
        onSearch={onSearch}
        enterButton
        allowClear
      />
    </Tooltip>

  );
}

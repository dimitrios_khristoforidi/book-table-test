import { useEffect } from 'react';
import { Space, Table, Tag } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import SearchField from '../SearchField';
import {
  booksList,
  booksListLoading,
  booksListPagination,
  booksSearchList,
} from '../../store/selectors/booksSelectors';
import { getBooksList } from '../../store/actions/booksTableActions';
import { openModal } from '../../store/actions/bookModalActions';
import './index.css';

export default function BooksTable() {
  const dispatch = useDispatch();
  const loading = useSelector(booksListLoading);
  const bookPagination = useSelector(booksListPagination);
  const list = useSelector(booksList);
  const searchList = useSelector(booksSearchList);

  const onTableChange = (pagination) => {
    dispatch(getBooksList(pagination));
  };

  useEffect(() => {
    onTableChange(bookPagination);
  }, []);

  const columns = [
    {
      title: '№',
      dataIndex: 'id',
      key: 'bookTableId',
      width: 150,
      render: (id) => <p className="book" onClick={() => dispatch(openModal(id))}>{id}</p>,
    },
    {
      title: 'Название',
      dataIndex: 'name',
      key: 'bookTableName',
    },
    {
      title: 'Тэги',
      dataIndex: 'keywords',
      key: 'bookTableTags',
      render: (keywords) => [...keywords].map((keyword) => (
        <Tag color="blue">
          {keyword.toUpperCase()}
        </Tag>
      )),
    },
    {
      title: 'Дата создания',
      dataIndex: 'createdDate',
      key: 'bookTableAction',
      width: 200,
    },
  ];

  return (
    <Space direction="vertical" size={48}>
      <SearchField />
      <Table
        columns={columns}
        dataSource={searchList.length ? searchList : list}
        scroll={{ y: 500 }}
        loading={loading}
        pagination={searchList.length ? null : bookPagination}
        onChange={onTableChange}
      />
    </Space>
  );
}

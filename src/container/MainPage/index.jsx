import BooksTable from '../../component/Table';
import BookModal from '../../component/BookModal';

export default function MainPage() {
  return (
    <>
      <BooksTable />
      <BookModal />
    </>
  );
}

import {
  CLOSE_MODAL,
  OPEN_MODAL,
  OPEN_MODAL_ERROR,
  OPEN_MODAL_SUCCESS,
} from '../types/bookModalTypes';

export const openModal = (id) => ({
  type: OPEN_MODAL,
  payload: id,
});

export const openModalSuccess = (data) => ({
  type: OPEN_MODAL_SUCCESS,
  payload: data,
});

export const openModalError = (error) => ({
  type: OPEN_MODAL_ERROR,
  payload: error,
});

export const closeModal = () => ({
  type: CLOSE_MODAL,
});

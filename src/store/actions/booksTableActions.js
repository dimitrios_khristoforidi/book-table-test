import {
  GET_BOOKS_LIST,
  GET_BOOKS_LIST_ERROR,
  GET_BOOKS_LIST_SUCCESS,
  GET_BOOKS_SEARCH,
} from '../types/booksTableTypes';

export const getBooksList = (pagination) => ({
  type: GET_BOOKS_LIST,
  pagination,
});

export const getBooksListSuccess = (data) => ({
  type: GET_BOOKS_LIST_SUCCESS,
  payload: data,
});

export const getBooksListError = (error) => ({
  type: GET_BOOKS_LIST_ERROR,
  payload: error,
});

export const getBooksSearch = (searchTerm) => ({
  type: GET_BOOKS_SEARCH,
  payload: searchTerm,
});

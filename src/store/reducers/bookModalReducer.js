import {
  CLOSE_MODAL,
  OPEN_MODAL,
  OPEN_MODAL_ERROR,
  OPEN_MODAL_SUCCESS,
} from '../types/bookModalTypes';
import { getNewState } from '../stateUpdater';

const initialState = {
  show: false,
  loading: false,
  error: null,
  data: null,
};

const BookModalReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MODAL:
      return getNewState(state, { show: true, loading: true });
    case OPEN_MODAL_SUCCESS:
      return getNewState(state, { loading: false, data: action.payload });
    case OPEN_MODAL_ERROR:
      return getNewState(state, { loading: false, error: action.payload });
    case CLOSE_MODAL:
      return initialState;
    default:
      return state;
  }
};

export default BookModalReducer;

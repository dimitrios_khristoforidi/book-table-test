import { getNewState, searchBook } from '../stateUpdater';
import {
  GET_BOOKS_LIST,
  GET_BOOKS_LIST_ERROR,
  GET_BOOKS_LIST_SUCCESS,
  GET_BOOKS_SEARCH,
} from '../types/booksTableTypes';

const initialState = {
  loading: false,
  error: null,
  searchTerm: '',
  pagination: {
    current: 1,
    pageSize: 10,
    total: null,
  },
  list: [],
  searchList: [],
};

const BooksTableReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BOOKS_LIST:
      return getNewState(state, { loading: true, pagination: action.pagination });
    case GET_BOOKS_LIST_SUCCESS:
      const { illustrationData, pagination } = action.payload;
      return getNewState(
        state,
        {
          loading: false,
          list: illustrationData,
          pagination: {
            ...state.pagination,
            current: pagination.currentPage,
            total: pagination.allCount,
          },
        },
      );
    case GET_BOOKS_LIST_ERROR:
      return getNewState(state, { loading: false, error: action.payload });
    case GET_BOOKS_SEARCH:
      return getNewState(
        state,
        {
          searchTerm: action.payload,
          searchList: searchBook(state.list, action.payload),
        },
      );
    default:
      return state;
  }
};

export default BooksTableReducer;

import { combineReducers } from 'redux';
import BooksTableReducer from './reducers/booksTableReducer';
import BookModalReducer from './reducers/bookModalReducer';

const rootReducer = combineReducers({
  BooksTableReducer,
  BookModalReducer,
});

export default rootReducer;

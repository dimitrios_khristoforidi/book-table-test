import { all } from 'redux-saga/effects';
import { BooksTableSaga } from './sagas/booksTableSaga';
import { BookModalSaga } from './sagas/bookModalSaga';

export default function* rootSaga() {
  yield all([
    BooksTableSaga(),
    BookModalSaga(),
  ]);
}

import { call, put, takeEvery } from 'redux-saga/effects';
import { OPEN_MODAL } from '../types/bookModalTypes';
import { openModalError, openModalSuccess } from '../actions/bookModalActions';
import booksAPI from '../../API/booksAPI';

export function* openSingleBookModal({ payload }) {
  try {
    const data = yield call(booksAPI.getSingleBook, payload);
    yield put(openModalSuccess(data.result.illustrationData));
  } catch (e) {
    yield put(openModalError(e.response));
  }
}

export function* BookModalSaga() {
  yield takeEvery(OPEN_MODAL, openSingleBookModal);
}

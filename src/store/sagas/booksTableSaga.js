import { call, put, takeEvery } from 'redux-saga/effects';
import { getBooksListError, getBooksListSuccess } from '../actions/booksTableActions';
import { GET_BOOKS_LIST } from '../types/booksTableTypes';
import booksAPI from '../../API/booksAPI';

export function* getListOfBooks({ payload, pagination }) {
  try {
    const data = yield call(booksAPI.getBooksList, payload, pagination);
    yield put(getBooksListSuccess(data.result));
  } catch (e) {
    console.log(e);
    yield put(getBooksListError(e.response));
  }
}

export function* BooksTableSaga() {
  yield takeEvery(GET_BOOKS_LIST, getListOfBooks);
}

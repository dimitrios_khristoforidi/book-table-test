export const booksList = (state) => state.BooksTableReducer.list;
export const booksSearchList = (state) => state.BooksTableReducer.searchList;
export const booksListLoading = (state) => state.BooksTableReducer.loading;
export const booksSearchTerm = (state) => state.BooksTableReducer.searchTerm;
export const booksListPagination = (state) => state.BooksTableReducer.pagination;

export const bookData = (state) => state.BookModalReducer.data;
export const bookModalShow = (state) => state.BookModalReducer.show;
export const bookModalLoading = (state) => state.BookModalReducer.loading;

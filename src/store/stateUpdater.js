export const getNewState = (state, data) => ({ ...state, ...data });

export const searchBook = (list, term) => {
  if (term) {
    return list.filter(
      (item) => Number(item.id) === Number(term) || item.name.includes(term),
    );
  }
  return [];
};
